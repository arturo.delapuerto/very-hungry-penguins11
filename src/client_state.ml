(* default game_state *)
let gs = ref None


(* default value for port/addr *)
let port = ref 10234
let addr = ref "localhost"
let server_changed = ref false
let server_connected = ref false

(** game mode *)
type game_mode = Solo | Multi

let game_mode = ref Solo
let filename = ref ""


let init_connexion () =
  Utils._log ("client: trying to connect to server " ^ !addr ^
                " on port " ^ (string_of_int !port));
  try
    Network.connect_to_server !addr !port;
    Utils._log "client: connected to the server";
    server_changed := false;
    server_connected := true
  with Network.Network_error -> server_connected := false


let get_list_of_games () =
  if not !server_connected || !server_changed then
    init_connexion();
  (* TODO send GetListOfGames and wait response if connexion succeded *)
  []
